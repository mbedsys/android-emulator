#!/bin/bash

ANDROID_EMULATOR_API=${ANDROID_EMULATOR_API:-"android-25"}
ANDROID_EMULATOR_ARCH=${ANDROID_EMULATOR_ARCH:-"x86"}

# Detect ip and forward ADB ports outside to outside interface
ip=$(ip addr | awk '/inet / { if ($2 != "127.0.0.1/8") { gsub(/\/.*/, "", $2); print $2; exit } }')
socat tcp-listen:5037,bind=$ip,fork tcp:127.0.0.1:5037 &
socat tcp-listen:5554,bind=$ip,fork tcp:127.0.0.1:5554 &
socat tcp-listen:5555,bind=$ip,fork tcp:127.0.0.1:5555 &

emulator_args=()
qemu_args=()
for arg in "$@"; do
    case "$arg" in
        -h|--help)
            echo "Usage: docker run --privileged -e ANDROID_EMULATOR_API=$ANDROID_EMULATOR_API -e ANDROID_EMULATOR_ARCH=$ANDROID_EMULATOR_ARCH android-emulator [emulator_args... [-qemu qemu_args...]]"
            exit
            ;;
        -qemu)
            arg_type="qemu"
            ;;
        *)
            case "$arg_type" in
                qemu)
                    qemu_args+=("$arg")
                    ;;
                *)
                    emulator_args+=("$arg")
                    ;;
            esac
            ;;
    esac
done

echo "no" | ${ANDROID_HOME}/tools/bin/avdmanager create avd --force --name test --abi google_apis/${ANDROID_EMULATOR_ARCH} \
    --package "system-images;$ANDROID_EMULATOR_API;google_apis;${ANDROID_EMULATOR_ARCH}"
echo "no" | ${ANDROID_HOME}/tools/emulator -avd test -noaudio -no-window -gpu off -verbose "${emulator_args[@]}" -qemu -usbdevice tablet -vnc :0 "${qemu_args[@]}"
